# Project: Orhan - WCB (Wheelchair Control Box), version 2

## Description
Project "Orhan" is focusing on custom control possibilities for electric powered wheelchairs. This because not everybody can drive the wheelchair by onboard standard joystick (depending on the situation of the disabled person). "Orhan" is the general name of the project which can have multiple hardware devices to achieve a goal together: Controlling the wheelchair.

This part is so called the "Wheelchair Control Box", which is the joystick replacer. The WCB is responsible for:

* Communicating with the "Motor Control Box"
* Reading inputs from driver
* Supports Bluetooth Low Energy (BLE) communication so the wheelchair can be controlled by PC/smartphone/tablet

## Hardware
For now the project has been started with the following hardware:

* nRF52840 development board from Nordic Semiconductor. It contains a ARM Cortex-M4 microcontroller.

## Software


## Note
Project "Orhan" is a educative hobby project and comes with ABSOLUTELY NO WARRANTY, to the extent permitted by applicable law.

## License

